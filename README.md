# Calculator App

A simple Calculator App without using Third-Party-Libraries.
The Calculator can compute simple arithmetic instructions.

![App View](https://gitlab.com/app-development8/android/calculator/-/raw/main/app/src/main/res/drawable/demo_app.png)
